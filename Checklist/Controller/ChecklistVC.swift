//
//  ViewController.swift
//  Checklist
//
//  Created by amr on 3/1/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

class ChecklistVC: UITableViewController , ItemDetailsVCDelegate {
    
        var items : [ChecklistItem] = [ChecklistItem(text: "walk the dog", checked: true), ChecklistItem(text: "Brush my teeth", checked: false),ChecklistItem(text: "Play football", checked: true),ChecklistItem(text: "Watch TV", checked: true)]
    
    
    //MARK: - PROTOCOL FUNCTIONS
    //*****************************************************
    func addItemVCDidCancel(_ controller: ItemDetailsVC) {
        navigationController?.popViewController(animated: true)
    }
    
    func addItemVC(_controller: ItemDetailsVC, didFinishAdding item: ChecklistItem) {
        navigationController?.popViewController(animated: true)
        items.append(item)
        tableView.reloadData()
    }
    func addItemVC(_controller: ItemDetailsVC, didFinishEditing item: ChecklistItem) {
        if let index = items.index(of: item)
        {
           items[index].text = item.text
            tableView.reloadData()
            print("\(index) wel item \(item.text)")
        }
        navigationController?.popViewController(animated: true)
    }

  
    

    
 

    //MARK: - VIEWDIDLOAD
    //*****************************************************
    override func viewDidLoad() {
        super.viewDidLoad()
        
       navigationController?.navigationBar.prefersLargeTitles = true
    }
    
   
    //MARK: - TABLE VIEW FUNCTIONS
    //*****************************************************
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            
            items[indexPath.row].checked = !items[indexPath.row].checked

            
            configureCheckmark(for: cell, at: indexPath)
            
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChecklistItem", for: indexPath)
        let label = cell.viewWithTag(1000) as! UILabel
        label.text = items[indexPath.row].text

        
        configureCheckmark(for: cell, at: indexPath)
        return cell
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
      items.remove(at: indexPath.row)
        tableView.reloadData()
    }
    
    
    //MARK: - PREPARE FOR SEGUES
    //*****************************************************
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Additem" {
            let additemVC = segue.destination as! ItemDetailsVC
            additemVC.delegate = self
        }
        else if segue.identifier == "EditItem"
        {
            let edititemVC = segue.destination as! ItemDetailsVC
            edititemVC.delegate = self
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell){
             edititemVC.itemToEdit = items[indexPath.row]
            }
            
        }
    }
    
    
    //MARK: - CUSTOM FUNCTIONS
    //*****************************************************
    func configureCheckmark(for cell: UITableViewCell, at indexPath: IndexPath) {
        let label = cell.viewWithTag(1001) as! UILabel
        if items[indexPath.row].checked {
            label.text = "✔️"
        } else {
            label.text = ""
        }
        
    }
    
    
    @IBAction func addBtnClicked(_ sender: Any) {
        // let newRowIndex = items.count
      //  var titles : [String] = ["Play tennis" , "Watch premier league" , "Go bowling", "Get a life"]
     //   var randomNumber = arc4random_uniform(UInt32(titles.count))
      //  let item = ChecklistItem(text: titles[Int(randomNumber)], checked: false)
//items.append(item)
//        let indexPath = IndexPath(row: newRowIndex, section: 0)
//        let indexPaths = [indexPath]
//        tableView.insertRows(at: indexPaths, with: .automatic)
       // tableView.reloadData()
        
        
    }
    
    
}

