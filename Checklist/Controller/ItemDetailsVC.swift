//
//  AddItemsVC.swift
//  Checklist
//
//  Created by amr on 3/2/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

//MARK: - CREATING PROTOCOL
//*****************************************************
protocol ItemDetailsVCDelegate: class {
    func addItemVCDidCancel(_ controller : ItemDetailsVC)
    func addItemVC(_controller: ItemDetailsVC, didFinishAdding item : ChecklistItem)
    func addItemVC(_controller: ItemDetailsVC, didFinishEditing item : ChecklistItem)
}



class ItemDetailsVC: UITableViewController, UITextFieldDelegate {
    
    
    //MARK: - IBOUTLETS AND VARIABLES
    //*****************************************************
    
    
    @IBOutlet weak var doneBtn: UIBarButtonItem!
    @IBOutlet weak var itemNameTF: UITextField!
    var itemToEdit: ChecklistItem?
    
    weak var delegate: ItemDetailsVCDelegate?
    
    
    //MARK: - VIEWDIDLOAD & VIEWWILLAPPEAR
    //*****************************************************
    override func viewDidLoad() {
        super.viewDidLoad()

      navigationItem.largeTitleDisplayMode = .never
        itemNameTF.delegate = self
        
        if let item = itemToEdit {
            title = "Edit Item"
            itemNameTF.text = item.text
            doneBtn.isEnabled = true
        }
       
    }
    override func viewWillAppear(_ animated: Bool) {
        itemNameTF.becomeFirstResponder()
    }
    //MARK: - FUNCTIONS
    //*****************************************************
   
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
 
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let oldText = textField.text!
        let stringRange = Range(range, in:oldText)
        let newText = oldText.replacingCharacters(in: stringRange!, with: string)
        if newText.isEmpty {
            doneBtn.isEnabled = false
        }
        else {
            doneBtn.isEnabled = true
        }
        return true
        
    }
    //MARK: - IBACTIONS
    //*****************************************************
    @IBAction func doneClicked(_ sender: Any) {
        //        navigationController?.popViewController(animated: true)
        if let itemTodEdit = itemToEdit{
            itemTodEdit.text = itemNameTF.text!
            delegate?.addItemVC(_controller: self, didFinishEditing: itemTodEdit)
        }
        else {
            let item = ChecklistItem(text: itemNameTF.text!, checked: false)
            delegate?.addItemVC(_controller: self, didFinishAdding: item)
        }
    }
    @IBAction func cancelClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        delegate?.addItemVCDidCancel(self)
    }
}
