//
//  ChecklistItem.swift
//  Checklist
//
//  Created by amr on 3/1/18.
//  Copyright © 2018 amr. All rights reserved.
//

import Foundation
class ChecklistItem: NSObject {
    var text = ""
    var checked = false

    init (text :String, checked: Bool){
        self.text = text
        self.checked = checked
    }
}
